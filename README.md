## FluxCD Sample Setup
* Install Flux cli:
```
brew install fluxcd/tap/flux
```

* Set up environment variables:
```
source .env
export GITLAB_USER=${DATA_E2E_FLUX_GIT_USER}
export GITLAB_TOKEN=${DATA_E2E_FLUX_GIT_TOKEN}
export GITLAB_PROJECT=fluxtest # name of the pre-existing GitLab project
export K8s_CLUSTER=mlops #name assigned to the target Kubernetes cluster
```

* Create a directory structure for bootstrapping:
```
mkdir -p clusters/${K8s_CLUSTER}/
touch clusters/${K8s_CLUSTER}/gotk-components.yaml \
    clusters/${K8s_CLUSTER}/gotk-sync.yaml \
    clusters/${K8s_CLUSTER}/kustomization.yaml
```

* Run the bootstrap for the cluster:
```
echo "$GITLAB_TOKEN" | flux bootstrap gitlab \
  --deploy-token-auth \
  --owner=${GITLAB_USER} \
  --repository=${GITLAB_PROJECT} \
  --branch=main \
  --read-write-key=true \
  --personal \
  --path=clusters/${K8s_CLUSTER}
```